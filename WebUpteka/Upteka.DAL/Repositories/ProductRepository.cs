﻿using System.Collections.Generic;
using Upteka.DAL.DB;
using Upteka.DAL.Entities;
using Upteka.DAL.Interfaces;

namespace Upteka.DAL.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private DBContext db;

        public ProductRepository(DBContext context) //TODO
        {
            db = context;
        }

        public IEnumerable<Product> GetAll()
        {
            return db.GetAll();
        }

        public Product Get(int id)
        {
            return db.Get(id);
        }

        public void Create(Product item)
        {
            db.Create(item);
        }

        public void Update(Product item)
        {                         
            db.Update(item);
        }

        public void Delete(Product item)
        {     
            db.Delete(item);
        }
    }
}
