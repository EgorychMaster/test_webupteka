﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Upteka.DAL.DB;
using Upteka.DAL.Entities;
using Upteka.DAL.Interfaces;

namespace Upteka.DAL.Repositories
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private DBContext db;
        private ProductRepository productRepository;

        public UnitOfWork(string connectionString)
        {
            db = new DBContext(connectionString);
        }
        public IRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);
                return productRepository;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
