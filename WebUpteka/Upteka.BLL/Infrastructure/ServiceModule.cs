﻿using Ninject.Modules;
using Upteka.BLL.Interfaces;
using Upteka.BLL.Services;
using Upteka.DAL.Interfaces;
using Upteka.DAL.Repositories;

namespace Upteka.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
