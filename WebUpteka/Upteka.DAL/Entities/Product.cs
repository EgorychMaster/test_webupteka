﻿using SQLite;

namespace Upteka.DAL.Entities
{
    public class Product
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Number { get; set; }
        public string Burcode { get; set; }
    }
}
