﻿using System;
using System.Collections.Generic;
using System.Web.Hosting;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using Upteka.BLL.DTO;
using Upteka.BLL.Infrastructure;
using Upteka.BLL.Interfaces;
using Upteka.WEB.Models;


namespace Upteka.WEB.Controllers
{
    public class HomeController : Controller
    {
        [Inject]
        public IServiceProduct service { get; set; }

        // GET: Home
        public ActionResult Index()
        {
            Mapper.CreateMap<ProductDTO, ProductViewModel>();
            var model = Mapper.Map<IEnumerable<ProductDTO>, List<ProductViewModel>>(service.GetAll());

            return View(model);
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            Mapper.CreateMap<ProductDTO, ProductViewModel>();
            var model = Mapper.Map<ProductDTO, ProductViewModel>(service.Get(id));

            return View(model);
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Настройка AutoMapper
                Mapper.CreateMap<ProductViewModel, ProductDTO>();
                // Выполняем сопоставление
                ProductDTO prod = Mapper.Map<ProductViewModel, ProductDTO>(model);
                service.Create(prod);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
                return HttpNotFound();
            // Настройка AutoMapper
            Mapper.CreateMap<ProductDTO, ProductViewModel>();
            // Выполняем сопоставление
            ProductViewModel model = Mapper.Map<ProductDTO, ProductViewModel>(service.Get(id));

            return View(model);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Настройка AutoMapper
                Mapper.CreateMap<ProductViewModel, ProductDTO>();
                // Выполняем сопоставление
                ProductDTO prod = Mapper.Map<ProductViewModel, ProductDTO>(model);
                service.Update(prod);

                return RedirectToAction("Index");
            }
            return View();
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ProductViewModel model)
        {
            if (id > 0)
            {
                // Настройка AutoMapper
                Mapper.CreateMap<ProductViewModel, ProductDTO>();
                // Выполняем сопоставление
                ProductDTO prod = Mapper.Map<ProductViewModel, ProductDTO>(model);
                service.Delete(prod);

                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
