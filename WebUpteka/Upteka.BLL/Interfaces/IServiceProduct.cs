﻿using System.Collections.Generic;
using Upteka.BLL.DTO;
using Upteka.DAL.Entities;

namespace Upteka.BLL.Interfaces
{
    public interface IServiceProduct
    {
        IEnumerable<ProductDTO> GetAll();
        ProductDTO Get(int id);
        void Create(ProductDTO item);
        void Update(ProductDTO item);
        void Delete(ProductDTO item);
    }
}
