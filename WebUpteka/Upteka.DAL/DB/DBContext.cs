﻿using System.Collections.Generic;
using SQLite;
using Upteka.DAL.Entities;

namespace Upteka.DAL.DB
{
    public class DBContext
    {
        string connectionString; //TODO

        public DBContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<Product> GetAll()
        {
            using (var db = GetConnection())
            {
                var products = db.Query<Product>("SELECT * FROM Product");
                //                var products = db.Table<Product>();
                return products;
            }
        }

        public Product Get(int id)
        {
            using (var db = GetConnection())
            {
                var product = db.Get<Product>(id);
                return product;
            }
        }

        public void Create(Product item)
        {
            using (var db = GetConnection())
            {
                db.Insert(item);
            }
        }

        public void Update(Product item)
        {
            using (var db = GetConnection())
            {
                db.Update(item);
            }
        }

        public void Delete(Product item)
        {
            using (var db = GetConnection())
            {
                db.Delete(item);
            }
        }

        protected SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(connectionString);
        }
    }
}
