﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using Upteka.BLL.DTO;
using Upteka.BLL.Interfaces;
using Upteka.DAL.Entities;
using Upteka.DAL.Interfaces;

namespace Upteka.BLL.Services
{
    public class ProductService : IServiceProduct
    {
        private IUnitOfWork repo { get; set; }

        public ProductService(IUnitOfWork re)
        {
            repo = re;
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            Mapper.CreateMap<Product, ProductDTO>();
            return Mapper.Map<IEnumerable<Product>, IEnumerable<ProductDTO>>(repo.Products.GetAll());
        }

        public ProductDTO Get(int id)
        {
            Mapper.CreateMap<Product, ProductDTO>();
            return Mapper.Map<Product,ProductDTO>(repo.Products.Get(id));
        }

        public void Create(ProductDTO item)
        {
            Mapper.CreateMap<ProductDTO, Product >();
            Product prod = Mapper.Map<ProductDTO, Product>(item);
            repo.Products.Create(prod);
        }

        public void Update(ProductDTO item)
        {
            Mapper.CreateMap<ProductDTO, Product>();
            Product prod = Mapper.Map<ProductDTO, Product>(item);
            repo.Products.Update(prod);
        }

        public void Delete(ProductDTO item)
        {
            Mapper.CreateMap<ProductDTO, Product>();
            Product prod = Mapper.Map<ProductDTO, Product>(item);
            repo.Products.Delete(prod);
        }
    }
}
