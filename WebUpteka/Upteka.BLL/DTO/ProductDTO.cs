﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Upteka.BLL.DTO
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public string Burcode { get; set; }
    }
}
