﻿using System.ComponentModel.DataAnnotations;

namespace Upteka.WEB.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
        public int Number { get; set; }
        public string Burcode { get; set; }
    }
}