﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Upteka.BLL.Services;
using Upteka.DAL.Interfaces;
using Upteka.DAL.Repositories;
using Upteka.WEB.Controllers;
using Upteka.WEB.Models;

namespace Upteka.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            List<ProductViewModel> model = new List<ProductViewModel>()
            {
                new ProductViewModel()
                {
                    Id = 1, Name = "Вася", Number = 33, Burcode = "rrrrrrrr"
                }
            };

            string path = AppDomain.CurrentDomain.BaseDirectory + @"/database/uptekaDB.db";

            IUnitOfWork repo = new UnitOfWork(path);
            controller.service = new ProductService(repo);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result.Model);
        }
    }
}
